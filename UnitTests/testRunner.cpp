/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */


#include <CppUTest/TestHarness.h>

extern "C" {
#include "timer2.h"
#include "mem_map.h"
}



TEST_GROUP(TimerTest)
{
	void setup()
	{

	}

	void teardown()
	{

	}
};


TEST(TimerTest,SetTimerOn){

	/* Clear all bits in T2CON */
	T2CON = 0x0000;

	Timer2control(TOn);

	CHECK_EQUAL(0x8000, T2CON);

}

TEST(TimerTest,ResetTimer){

	/* Set TON bit */
	T2CONbits.ON = 1;

	Timer2control(TOff);

	CHECK_EQUAL(0x0000, T2CON);

}
