/* 
 * File:   newmain.c
 * Author: pf
 *
 * Created on 19 February 2020, 16:54
 */

#include "config_bits.h"

#include <xc.h>

#include <stdint.h>

#include "timer2.h"

/*
 * 
 */
int main(int argc, char** argv) {

    int8_t result = 0;

    TRISAbits.TRISA3 = 0;
    TRISCbits.TRISC1 = 0;

    LATCbits.LATC1 = 0;

    // config_timer2(256,15500);


    result = set_timer2_freq(256, 10);

    if (result != 0) {
        LATCbits.LATC1 = 1;
        while (1) {
        }
    }

    Timer2control(TOn);

    while (1) {
        while (IFS0bits.T2IF == 0);
        IFS0bits.T2IF = 0;

        PORTAINV = 0xFF;

    }

    return (EXIT_SUCCESS);
}


